document.addEventListener(
  "DOMContentLoaded",
  function() {
    const sections = document.querySelectorAll(".menu__target");
    const menu_links = document.querySelectorAll(".menu__link");

    const makeActive = link => menu_links[link].classList.add("active");
    const removeActive = link => menu_links[link].classList.remove("active");
    const removeAllActive = () =>
      [...Array(sections.length).keys()].forEach(link => removeActive(link));
    const sectionMargin = 225;
    let currentActive = 0;

    window.addEventListener("scroll", () => {
      let current =
        sections.length -
        [...sections]
          .reverse()
          .findIndex(
            section => window.scrollY >= section.offsetTop - sectionMargin
          ) -
        1;

      if (current === sections.length) current = 0;
      if (current !== currentActive) {
        removeAllActive();
        currentActive = current;
        makeActive(current);
      }
    });
  },
  false
);
